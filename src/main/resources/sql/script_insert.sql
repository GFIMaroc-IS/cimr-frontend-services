insert into CIMR_CONTRAT_DEV.REF_PRODUIT(ID_PRODUIT,code,libelle) values(1,'01','AL KAMIL');
insert into CIMR_CONTRAT_DEV.REF_PRODUIT(ID_PRODUIT,code,libelle) values(2,'02','AL MOUNASSIB');
insert into CIMR_CONTRAT_DEV.REF_PRODUIT(ID_PRODUIT,code,libelle) values(3,'03','Tranche CNSS');
insert into CIMR_CONTRAT_DEV.REF_PRODUIT(ID_PRODUIT,code,libelle) values(4,'04','Complémentaire légale');
insert into CIMR_CONTRAT_DEV.REF_PRODUIT(ID_PRODUIT,code,libelle) values(5,'05','AL MOUSTAKBAL GROUPE (AG)');
insert into CIMR_CONTRAT_DEV.REF_PRODUIT(ID_PRODUIT,code,libelle) values(6,'06','AL MOUSTAKBAL INDIVIDUEL');
insert into CIMR_CONTRAT_DEV.REF_PRODUIT(ID_PRODUIT,code,libelle) values(7,'07','Retraite par capitalisation groupe (RCG)');

insert into CIMR_CONTRAT_DEV.REF_CATEGORIE_HABILITATION(ID_CATEGORIE, code, libelle) values(1,'01','categorie 1');
insert into CIMR_CONTRAT_DEV.REF_CATEGORIE_HABILITATION(ID_CATEGORIE, code, libelle) values(2,'02','categorie 2');

insert into CIMR_CONTRAT_DEV.REF_MODE_PAIEMENT(ID_MODE_PAIEMENT,CODE,LIBELLE) VALUES(1,'01','Prélèvement automatique');
insert into CIMR_CONTRAT_DEV.REF_MODE_PAIEMENT(ID_MODE_PAIEMENT,CODE,LIBELLE) VALUES(2,'02','Ordre de virement');