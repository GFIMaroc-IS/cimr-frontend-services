CREATE TABLE ADHERENT(
    ID INT not null PRIMARY KEY AUTO_INCREMENT,
    LOGIN VARCHAR(255 ),
    PASSWORD VARCHAR(255 ),
    ID_ADHESION INT,
    FOREIGN KEY (ID_ADHESION) REFERENCES ADHESION (ID_ADHESION)
);