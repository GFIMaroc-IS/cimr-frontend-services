insert into REF_PRODUIT(ID_PRODUIT,code,libelle) values(1,'01','AL KAMIL');
insert into REF_PRODUIT(ID_PRODUIT,code,libelle) values(2,'02','AL MOUNASSIB');
insert into REF_PRODUIT(ID_PRODUIT,code,libelle) values(3,'03','Tranche CNSS');
insert into REF_PRODUIT(ID_PRODUIT,code,libelle) values(4,'04','Complémentaire légale');
insert into REF_PRODUIT(ID_PRODUIT,code,libelle) values(5,'05','AL MOUSTAKBAL GROUPE (AG)');
insert into REF_PRODUIT(ID_PRODUIT,code,libelle) values(6,'06','AL MOUSTAKBAL INDIVIDUEL');
insert into REF_PRODUIT(ID_PRODUIT,code,libelle) values(7,'07','Retraite par capitalisation groupe (RCG)');

insert into REF_MODE_PAIEMENT(ID_MODE_PAIEMENT,CODE,LIBELLE) VALUES(1,'01','Prélèvement automatique');
insert into REF_MODE_PAIEMENT(ID_MODE_PAIEMENT,CODE,LIBELLE) VALUES(2,'02','Ordre de virement');

insert into REF_CATEGORIE_HABILITATION(ID_CATEGORIE, code, libelle) values(1,'01','Affilié & Salaires');
insert into REF_CATEGORIE_HABILITATION(ID_CATEGORIE, code, libelle) values(2,'02','Paiement des cotisations');

