package ma.cimr.contrat.model;

import java.io.Serializable;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.OneToOne;
import javax.persistence.Table;

@Entity
@Table(name="ADHERENT")
public class Adherent implements Serializable{

	/**
	 * 
	 */
	private static final long serialVersionUID = 7957917462776342792L;

	@Id
	@Column(name="ID")
	@GeneratedValue(strategy=GenerationType.IDENTITY)
	private Long id;
	
	@Column(name="LOGIN")
	private String login;
	
	@Column(name="PASSWORD")
	private String password;
	
	@OneToOne(cascade=CascadeType.ALL)
	@JoinColumn(name = "ID_ADHESION", referencedColumnName = "ID_ADHESION")
	private Adhesion adhesion;
	
	public Adherent() {
		
	}
	
	public Adherent(String login, String password) {
		super();
		this.login = login;
		this.password = password;
	}
	
	public Adherent(String login, String password, Adhesion adhesion) {
		super();
		this.login = login;
		this.password = password;
		this.adhesion = adhesion;
	}

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public String getLogin() {
		return login;
	}

	public void setLogin(String login) {
		this.login = login;
	}

	public String getPassword() {
		return password;
	}

	public void setPassword(String password) {
		this.password = password;
	}

	public Adhesion getAdhesion() {
		return adhesion;
	}

	public void setAdhesion(Adhesion adhesion) {
		this.adhesion = adhesion;
	}

}
