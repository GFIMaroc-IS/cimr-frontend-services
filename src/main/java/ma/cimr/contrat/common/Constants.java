package ma.cimr.contrat.common;

public abstract class Constants {
	
	public static final String CONFIG_FILE = "config.properties";
	public static final String ERRORS = "errors";
	public static final String MAP_ERRORS = "mapErrors";
	public static final String NBR_LIGNES_ERRORS = "nbrLignesErrors";
	public static final String MESSAGES = "messages";
	public static final Integer DB_MAX_RESULTS =10;
	public static final String PAGE_DTO = "pageDtos";
	public static final String LIST_DTO = "listDtos";
	public static final String DTO = "dto";
	public static final String ID = "id";
	
	public static final String DATE_FORMAT = "yyyy-MM-dd";
	public static final String DATE_FORMAT_FLUX = "dd/MM/yyyy";
	
	public static final String XML_PATH = "src/main/resources/xml/";
	
	public static final String URL_BDOC_ON_DEMAND = "url.bdoc.on.demand";
	public static final String URL_BDOC_DOCUMENT_CONTENT = "url.bdoc.document.content";
	public static final String URL_BDOC_INTERACTIVE = "url.bdoc.interactive";
	
	public static final String URL_SERVICE_PRINT_CONVENTION = "url.service.print.convention";
	public static final String URL_SERVICE_PRINT_BULLETIN = "url.service.print.bulletin";
	
	public static final String CONVENTION_XML_FILE_NAME_PATH = "convention.xml.file.name.path";
	public static final String CONVENTION_XML_FILE_NAME_TEMPLATE = "convention.xml.file.name.template";
	
	public static final String STATUT_EN_COURS = "En cours";
	public static final String STATUT_A_VALIDER = "A valider";
	public static final String STATUT_VALIDE = "Validée";
	public static final String STATUT_REJETE = "Rejetée";
	
	public static final Integer STATUT_EN_COURS_CODE = 1;
	public static final Integer STATUT_A_VALIDER_CODE = 2;
	public static final Integer STATUT_VALIDE_CODE = 3;
	public static final Integer STATUT_REJETE_CODE = 4;
	
	public static final Integer PASSWORD_LENGTH = 8;
	
	public static final String LOGIN = "login";
	public static final String PASSWORD = "password";
	public static final String BYTES = "bytes";
	public static final String PARAMS_MAP = "parameters";
	public static final String CONTENT = "content";
	
	public final static String message_queue = "cimr_journalisation";
	
	public static final String USERNAME = "userName";
	public static final String FILE = "file";
	public static final String SIGN_PAGE_URL = "signPageUrl";

	public static final String BULLETIN_PDF_FILE_NAME_PATH = "bulletin.pdf.file.name.path";
	public static final String URL_SERVICE_ESIGN_BULLETIN = "url.service.esign.bulletin";
}
