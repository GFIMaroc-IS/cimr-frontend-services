package ma.cimr.contrat.common;

public class BusinessException extends Exception {

	private static final long serialVersionUID = 1840127865841561509L;
	private Throwable throwable; 
	
	public BusinessException(String message) {
		this.throwable = new Exception(message);
	}

	public BusinessException(String message, Throwable cause) {
		this.throwable = new Exception(message, cause);
	}

	public BusinessException(Throwable cause) {
		this.throwable = new Exception(cause);
	}

	public String getMessage() {
		return throwable.getMessage();
	}
}
