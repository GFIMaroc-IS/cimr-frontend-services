package ma.cimr.contrat.util;

public class StringUtil {

	public static boolean isEmpty(String str) {
		
		if(str == null) {
			return true;
		}
		if(str.trim().equals("")) {
			return true;
		}
		return false;
	}
	
	public static String nullIfEmpty(String str) {
		if(isEmpty(str))
			return null;
		return str;
	}
}
