package ma.cimr.contrat.dao;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.JpaSpecificationExecutor;
import org.springframework.stereotype.Repository;

import ma.cimr.contrat.model.Adhesion;

@Repository
public interface AdhesionRepository extends JpaRepository<Adhesion, Long>{
	
}
