package ma.cimr.contrat.dao;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;

import ma.cimr.contrat.model.Adherent;

@Repository
public interface AdherentRepository extends JpaRepository<Adherent, Long>{
	
	@Query("select u from Adherent u where u.login = :login")
	Adherent getAdherent(String login);
	
	@Query("select u from Adherent u where u.login = :login and u.password = :password")
	Adherent getAdherent(String login, String password);
}
