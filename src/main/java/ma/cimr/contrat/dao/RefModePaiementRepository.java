package ma.cimr.contrat.dao;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import ma.cimr.contrat.model.RefModePaiement;

@Repository
public interface RefModePaiementRepository extends JpaRepository<RefModePaiement, Long>{

	public RefModePaiement findByCode( String code);
}
	