package ma.cimr.contrat.dao.util;

import org.springframework.data.jpa.domain.Specification;

import ma.cimr.contrat.model.Adhesion;
import ma.cimr.contrat.model.Adhesion_;

public class AdhesionSpecs {
	
	public static Specification<Adhesion> getAdhesionByIce(String ice) {
		return (root, query, criteriaBuilder) -> {
			return criteriaBuilder.equal(root.get(Adhesion_.ice),ice);
        };
	}
}
