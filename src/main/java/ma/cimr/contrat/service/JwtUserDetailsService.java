package ma.cimr.contrat.service;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.security.core.userdetails.UsernameNotFoundException;
import org.springframework.stereotype.Service;

import ma.cimr.contrat.dao.AdherentRepository;
import ma.cimr.contrat.model.Adherent;
import ma.cimr.contrat.service.dto.AdherentDetails;


@Service
public class JwtUserDetailsService implements UserDetailsService {
	
	Logger logger = LoggerFactory.getLogger(JwtUserDetailsService.class);
	
	@Autowired
	private AdherentRepository adherentRepository;
	
//	@Autowired
//	private ProfilRepository profilRepository;
	
	
	@Override
	public UserDetails loadUserByUsername(String username) throws UsernameNotFoundException {
		
		logger.info("==== Début de la méthode loadUserByUsername username : "+ username + " ====");
		UserDetails userDetails = null;
		
		try {
			Adherent adherent = adherentRepository.getAdherent(username);
			if (adherent != null && adherent.getLogin() != null && !adherent.getLogin().isEmpty()) {
				
//				List<Profil> profils = profilRepository.getProfilsByUsername(username);
				
				userDetails = new AdherentDetails(username, adherent.getAdhesion().getId());
			}
		} catch(Exception e) {
			logger.error("Exception in method loadUserByUsername : " + username, e);
		}
		 
		return userDetails;
	}

}