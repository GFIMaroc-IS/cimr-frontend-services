package ma.cimr.contrat.service;

import java.util.Map;

public interface IAuthenticationService {

	public boolean checkUser(String username, String password);
	
	public Map<String,Object> saveUser(String username, String password, Long idAdhesion);
	
}
