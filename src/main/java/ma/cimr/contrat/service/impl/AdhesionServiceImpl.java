package ma.cimr.contrat.service.impl;

import java.io.File;
import java.io.FileOutputStream;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Optional;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.configurationprocessor.json.JSONObject;
import org.springframework.jms.annotation.EnableJms;
import org.springframework.jms.core.JmsTemplate;
import org.springframework.stereotype.Service;
import org.springframework.web.multipart.MultipartFile;

import ma.cimr.contrat.common.Constants;
import ma.cimr.contrat.config.ApplicationConfig;
import ma.cimr.contrat.dao.AdhesionRepository;
import ma.cimr.contrat.model.Adhesion;
import ma.cimr.contrat.model.Contrat;
import ma.cimr.contrat.service.IAdhesionService;
import ma.cimr.contrat.service.IAuthenticationService;
import ma.cimr.contrat.service.dto.AdhesionData;
import ma.cimr.contrat.service.dto.AdhesionDto;
import ma.cimr.contrat.service.dto.ContratDto;
import ma.cimr.contrat.service.dto.DelegataireDto;
import ma.cimr.contrat.service.mapper.AdhesionMapper;
import ma.cimr.contrat.service.mapper.ContratMapper;
import ma.cimr.contrat.util.ApiUtils;
import ma.cimr.contrat.util.DateUtil;
import ma.cimr.contrat.util.FileUtil;
import ma.cimr.contrat.util.PasswordUtil;
import ma.cimr.contrat.util.StringUtil;



@Service
@EnableJms
public class AdhesionServiceImpl implements IAdhesionService {

	private static final Logger logger = LoggerFactory.getLogger(AdhesionServiceImpl.class);

	@Autowired
	private AdhesionMapper adhesionMapper;

	@Autowired
	private JmsTemplate jmsTemplate;
	
	@Autowired
	private ContratMapper contratMapper;
	
	@Autowired
	private AdhesionRepository adhesionRepository;
	
	@Autowired
	private IAuthenticationService authenticationService;
	
	@Override
	public Long saveAdhesion(AdhesionData adhesionData, Byte[] rcFile, Byte[] iceFile, Byte[] ribFile) {
//		jmsTemplate.convertAndSend("cimr_journalisation", "test");
		
		
		Long id = null;
		
		AdhesionDto adhesionDto = adhesionData.getAdhesion();
		Adhesion adhesion = adhesionMapper.toEntity(adhesionDto);
		if(rcFile != null && rcFile.length > 0 ) {
			adhesion.setRcFile(rcFile);
		}
		if(iceFile != null && iceFile.length > 0 ) {
			adhesion.setIceFile(iceFile);
		}
		
		ContratDto contratDto = adhesionData.getContrat();
		// delete empty delegataires
		List<DelegataireDto> list = DelegataireDto.list(contratDto.getDelegataires());
		contratDto.setDelegataires(list);
		
		Contrat contrat = contratMapper.toEntity(contratDto);
		
		contrat.setStatut(Constants.STATUT_EN_COURS_CODE);
		
		contrat.getDelegataires().forEach(d -> d.setContrat(contrat));
		contrat.getDelegataires().forEach(d -> {if(d.getCategorieHabilitation() == null || d.getCategorieHabilitation().getId() == null) d.setCategorieHabilitation(null);});
		
		if(ribFile != null && ribFile.length > 0 ) {
			contrat.setRibFile(ribFile);
		}
		
		if(contrat.getProduit() == null || contrat.getProduit().getId() == null)
			contrat.setProduit(null);
		
		if(contrat.getModePaiement() == null || contrat.getModePaiement().getId() == null)
			contrat.setModePaiement(null);
		
		contrat.setAdhesion(adhesion);
		
		adhesion.setContrat(contrat);
		
		Adhesion entity_ = adhesionRepository.save(adhesion);
		
		if(entity_ != null) {
			id = entity_.getId();
		}
		
		
		return id;
	}

	@Override
	public byte[] printConvention(Long idAdhesion) throws Exception {

		byte[] bytes = null;
		JSONObject paramsMap = new JSONObject();
		Map<String, String> parameters = new HashMap<>();
		
		Adhesion adhesion = adhesionRepository.getOne(idAdhesion);

		if (adhesion != null) {
			parameters.put("denomination", !StringUtil.isEmpty(adhesion.getNumRC()) ? adhesion.getNumRC() : "");
			parameters.put("rs", !StringUtil.isEmpty(adhesion.getRaisonSociale()) ? adhesion.getRaisonSociale() : "");
			parameters.put("lieu", !StringUtil.isEmpty(adhesion.getAdresse()) ? adhesion.getAdresse() : "");
			parameters.put("date_cse", adhesion.getContrat() != null && adhesion.getContrat().getDateEffetSouscription() != null ? DateUtil.dateToString(adhesion.getContrat().getDateEffetSouscription(), Constants.DATE_FORMAT_FLUX) : "");
			parameters.put("marque", "CIMR");
			parameters.put("langue", "FR");
			parameters.put("nom", !StringUtil.isEmpty(adhesion.getNomGestionnaire()) ? adhesion.getNomGestionnaire() : "");
			parameters.put("prenom", !StringUtil.isEmpty(adhesion.getPrenomGestionnaire()) ? adhesion.getPrenomGestionnaire() : "");

			paramsMap.put(Constants.PARAMS_MAP, parameters);
			
			String url = ApplicationConfig.getUrlServicePrintConvention();
			JSONObject response = ApiUtils.post(parameters, url);
			
			if(response != null && response.get(Constants.CONTENT) != null) {
				String content = (String)response.get(Constants.CONTENT);
				bytes = FileUtil.decodeFileToBase64(content);
			}
		}

		return bytes;
	}
	
	@Override
	public String printBulletin(Long idAdhesion) throws Exception {
	
		byte[] bytes = null;
		String content = null;
		JSONObject paramsMap = new JSONObject();
		Map<String, String> parameters = new HashMap<String, String>();
		
		Adhesion adhesion = adhesionRepository.getOne(idAdhesion);

		if (adhesion != null) {
			parameters.put("denomination", !StringUtil.isEmpty(adhesion.getNumRC()) ? adhesion.getNumRC() : "");
			parameters.put("rs", !StringUtil.isEmpty(adhesion.getRaisonSociale()) ? adhesion.getRaisonSociale() : "");
			parameters.put("lieu", !StringUtil.isEmpty(adhesion.getAdresse()) ? adhesion.getAdresse() : "");
			parameters.put("date_cse", adhesion.getContrat() != null && adhesion.getContrat().getDateEffetSouscription() != null ? DateUtil.dateToString(adhesion.getContrat().getDateEffetSouscription(), Constants.DATE_FORMAT_FLUX) : "");
			parameters.put("marque", "CIMR");
			parameters.put("langue", "FR");
			parameters.put("nom", !StringUtil.isEmpty(adhesion.getNomGestionnaire()) ? adhesion.getNomGestionnaire() : "");
			parameters.put("prenom", !StringUtil.isEmpty(adhesion.getPrenomGestionnaire()) ? adhesion.getPrenomGestionnaire() : "");

		}
		
		paramsMap.put(Constants.PARAMS_MAP, parameters);
		
		String url = ApplicationConfig.getUrlServicePrintBulletin();
		JSONObject response = ApiUtils.post(parameters, url);
		
		if(response != null && response.get(Constants.CONTENT) != null) {
			content = (String)response.get(Constants.CONTENT);
			bytes = FileUtil.decodeFileToBase64(content);
		}
		
		Files.write(Paths.get(ApplicationConfig.getBulletinPdfFileNamePath()), bytes);
		return content;
	}

	@Override
	public Map<String, Object> uploadRC(MultipartFile file) {
		
		Map<String, Object> response = new HashMap<String, Object>();

		
		
//		response = OCRisationService.readFile(file);
		
		response.put("numRC", "93435");
		response.put("raisonSociale", "INETUM CONSULTING MAROC");
		response.put("adresse", "1100 BD AL QODS PARC CASANEARSHORE SHORE 28 PLATEAU 302 SIDI MAAROUF , Casablanca>");
		response.put("formeJuridique", "SOCIETE ANONYME");
		
		return response;
	}

	@Override
	public Map<String, Object> uploadICE(MultipartFile file) {

		Map<String, Object> response = new HashMap<String, Object>();
		
//		response = OCRisationService.readFile(file);
		
		response.put("ice", "1003997");
		response.put("ifu", "001525571000051");
		response.put("cnss", "6005012");
		response.put("numTaxe", "36104563");
		
		return response;
	}

	@Override
	public Map<String, Object> uploadRIB(MultipartFile file) {
		
		Map<String, Object> response = new HashMap<String, Object>();
		
//		response = OCRisationService.readFile(file);
		
		response.put("titulaire", "Inetum Consulting Maroc");
		response.put("rib", "022780000140000502385174");
		
		return response;
	}

	@Override
	public Map<String, Object> generateCredentials(Long idAdhesion) {
		
		Map<String, Object> credentials = null;
		

		Optional<Adhesion> adhesionOpt = adhesionRepository.findById(idAdhesion);
		if(adhesionOpt.isPresent()) {
			Adhesion adhesion = adhesionOpt.get();
			String login = adhesion.getEmailGestionnaire();
			String password = PasswordUtil.getPassword(Constants.PASSWORD_LENGTH);
			
			credentials = authenticationService.saveUser(login, password, idAdhesion);
			
		}
		return credentials;
	}

	@Override
	public Map<String, Object> signBulletin(Long idAdhesion) throws Exception {
	
		Map<String, Object> parameters = new HashMap<String, Object>();
		Map<String, Object> result = new HashMap<String, Object>();
		String username = "";
		
		Adhesion adhesion = adhesionRepository.getOne(idAdhesion);
		
		if (adhesion != null) {
			username = (adhesion.getPrenomGestionnaire() != null ? adhesion.getPrenomGestionnaire() : "") + " " +
					(adhesion.getNomGestionnaire() != null ? adhesion.getNomGestionnaire() : "");
		}
		
		String fileContent = printBulletin(idAdhesion);
		
		parameters.put(Constants.USERNAME, username);
		parameters.put(Constants.FILE, fileContent);

		String url = ApplicationConfig.getUrlServiceESignBulletin();
		JSONObject response = ApiUtils.post(parameters, url);
		
		if(response != null) {
			result.put(Constants.ID, (String)response.get(Constants.ID));
			result.put(Constants.SIGN_PAGE_URL, (String)response.get(Constants.SIGN_PAGE_URL));
		}
		
		return result;
	}

	@Override
	public byte[] printSignedBulletin(String idBulletin) throws Exception {

		byte[] bytes = null;
		
		String url = ApplicationConfig.getUrlServiceESignBulletin() + "?id=" + idBulletin;
		bytes = ApiUtils.getFileContent(url);
					
		return bytes;
	}

}
