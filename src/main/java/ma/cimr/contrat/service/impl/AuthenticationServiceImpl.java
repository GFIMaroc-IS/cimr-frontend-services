package ma.cimr.contrat.service.impl;

import java.util.Base64;
import java.util.HashMap;
import java.util.Map;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import ma.cimr.contrat.common.Constants;
import ma.cimr.contrat.dao.AdherentRepository;
import ma.cimr.contrat.dao.AdhesionRepository;
import ma.cimr.contrat.model.Adherent;
import ma.cimr.contrat.model.Adhesion;
import ma.cimr.contrat.service.IAuthenticationService;

@Service
public class AuthenticationServiceImpl implements IAuthenticationService {

	private static final Logger logger = LoggerFactory.getLogger(AuthenticationServiceImpl.class);
	
	@Autowired
	private AdherentRepository adherentRepository;
	
	@Autowired
	private AdhesionRepository adhesionRepository;

	@Override
	public boolean checkUser(String username, String password) {
		
		byte[] bytesEncoded = Base64.getEncoder().encode(password.getBytes());
		String passwordEncoded = new String(bytesEncoded);
		logger.info("Password encoded '" + password + "' - '" + passwordEncoded + "'");
		Adherent adherent = adherentRepository.getAdherent(username, passwordEncoded);
		
		if(adherent != null)
			return true;
		
		return false;
	}

	@Override
	public Map<String,Object> saveUser(String username, String password, Long idAdhesion) {
		
		Map<String,Object> credentials = null;
		
		byte[] bytesEncoded = Base64.getEncoder().encode(password.getBytes());
		String passwordEncoded = new String(bytesEncoded);
		Adhesion adhesion = adhesionRepository.getOne(idAdhesion);
		Adherent adherent = new Adherent(username, passwordEncoded, adhesion);
		
		Adherent entity = adherentRepository.save(adherent);
		if(entity != null && entity.getId() != null) {
			
			credentials = new HashMap<String, Object>();
			
			credentials.put(Constants.LOGIN, username);
			credentials.put(Constants.PASSWORD, passwordEncoded);
		}
			
		return credentials;
	}


}
