package ma.cimr.contrat.service.dto;

import java.util.Date;

public class JournalisationDto {

	private Long id;
	private String action;
	private Date dateAction;
	
    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

	public String getAction() {
		return action;
	}

	public void setAction(String action) {
		this.action = action;
	}

	public Date getDateAction() {
		return dateAction;
	}

	public void setDateAction(Date dateAction) {
		this.dateAction = dateAction;
	}
}

