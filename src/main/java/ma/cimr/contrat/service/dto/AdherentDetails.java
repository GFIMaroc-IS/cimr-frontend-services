package ma.cimr.contrat.service.dto;

import java.util.ArrayList;
import java.util.Collection;
import java.util.List;

import org.springframework.security.core.GrantedAuthority;
import org.springframework.security.core.userdetails.UserDetails;

public class AdherentDetails implements UserDetails {
    
	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	private String username;
	private String password;
	private Long idAdhesion;
//	private List<Profil> listProfils;
	
	public AdherentDetails(String username) {
		this.username = username;
	}
	
	public AdherentDetails(String username, String password) {
		this.username = username;
		this.password = password;
	}
	
	public AdherentDetails(String username, Long idAdhesion) {
		this.username = username;
		this.idAdhesion = idAdhesion;
	}
	
//	public AdherentDetails(String username, List<Profil> listProfils) {
//		this.username = username;
//		this.listProfils = listProfils;
//	}
	
    @Override
    public Collection<? extends GrantedAuthority> getAuthorities() {
    	List<GrantedAuthority> list = new ArrayList<>();
//		for(Profil profil : this.listProfils)
//            list.add(new SimpleGrantedAuthority(profil.getLibelle()));
		return list;
    }

    @Override
    public String getPassword() {
        return this.password;
    }

    @Override
    public String getUsername() {
        return this.username;
    }

    @Override
    public boolean isAccountNonExpired() {
        return true;
    }

    @Override
    public boolean isAccountNonLocked() {
        return true;
    }

    @Override
    public boolean isCredentialsNonExpired() {
        return true;
    }

    @Override
    public boolean isEnabled() {
        return true;
    }

	public Long getIdAdhesion() {
		return idAdhesion;
	}

	public void setIdAdhesion(Long idAdhesion) {
		this.idAdhesion = idAdhesion;
	}

//	public List<Profil> getListProfils() {
//		return listProfils;
//	}
//
//	public void setListProfils(List<Profil> listProfils) {
//		this.listProfils = listProfils;
//	}

}
