package ma.cimr.contrat.service;

import java.util.Map;

import org.springframework.web.multipart.MultipartFile;

import ma.cimr.contrat.service.dto.AdhesionData;


public interface IAdhesionService {

	public byte[] printConvention(Long idAdhesion) throws Exception;
	
	public String printBulletin(Long idAdhesion) throws Exception;
	
	public Long saveAdhesion(AdhesionData adhesionData, Byte[] rcFile, Byte[] iceFile, Byte[] ribFile);

//	public byte[] modifyXml(Map<String, String> parameters);
	
	public Map<String, Object> uploadRC(MultipartFile file);
	
	public Map<String, Object> uploadICE(MultipartFile file);
	
	public Map<String, Object> uploadRIB(MultipartFile file);
	
	public Map<String, Object> generateCredentials(Long idAdhesion);
	
	public Map<String, Object> signBulletin(Long idAdhesion) throws Exception;
	
	public byte[] printSignedBulletin(String idBulletin) throws Exception;
}
