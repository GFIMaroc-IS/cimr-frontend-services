package ma.cimr.contrat.rest.Adhesion;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.springframework.stereotype.Component;

import ma.cimr.contrat.common.Constants;
import ma.cimr.contrat.service.ODMService;
import ma.cimr.contrat.service.dto.AdhesionData;
import ma.cimr.contrat.service.dto.AdhesionDto;

@Component
public class AdhesionValidator {
	
	public Map<String, Object> validateAdhesion(AdhesionData adhesionData) {
		
		Map<String, Object> result = new HashMap<>();
		List<String> errors = new ArrayList<>();
		
		Object response = ODMService.validateBusinessRules(adhesionData);
		
		
		result.put(Constants.ERRORS, errors);
		return result;
	}
}
