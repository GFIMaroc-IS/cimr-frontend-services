package ma.cimr.contrat.rest.Adhesion;

import java.io.IOException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.jms.JMSException;
import javax.servlet.http.HttpServletResponse;

import org.apache.commons.lang.ArrayUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.configurationprocessor.json.JSONObject;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.jms.annotation.EnableJms;
import org.springframework.jms.core.JmsMessagingTemplate;
import org.springframework.jms.core.JmsTemplate;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.multipart.MultipartFile;

import ma.cimr.contrat.common.Constants;
import ma.cimr.contrat.service.IAdhesionService;
import ma.cimr.contrat.service.dto.AdhesionData;
import ma.cimr.contrat.util.CollectionUtil;


@RestController
@RequestMapping("/api/adhesion")
@EnableJms
public class AdhesionController {

	private static final Logger logger = LoggerFactory.getLogger(AdhesionController.class);
	
	@Autowired
	private IAdhesionService adhesionService;
	
	@Autowired
	private AdhesionValidator adhesionValidator;
	
	@Autowired
	private JmsTemplate jmsTemplate;
	
	@Autowired
	private JmsMessagingTemplate jmsMessagingTemplate;
	
	private Byte[] rcFile;
	private Byte[] iceFile;
	private Byte[] ribFile;
	
	
	@GetMapping("/init")
    public void init() throws JMSException{
		logger.warn("Adhesion controler init called ");
		tracerAction("charger la page de la demande d'adhésion ");
		this.rcFile = null;
		this.iceFile = null;
		this.ribFile = null;
	}
	
	@PutMapping("/validate")
    public ResponseEntity<Map<String, Object>> validateAdhesion(@RequestBody AdhesionData adhesionData){
		
		Map<String, Object> response = new HashMap<>();
		List<String> errors = null;
		try {
			Map<String, Object> result = null;
			tracerAction("validate adhésion");
			
			result = adhesionValidator.validateAdhesion(adhesionData);
			
			if(result != null && result.get(Constants.ERRORS) != null) {
	        	errors = (List<String>)result.get(Constants.ERRORS);
	        	if(CollectionUtil.isNotEmpty(errors)) {
	        		response.put(Constants.ERRORS, errors);
	        		return new ResponseEntity<>(response, HttpStatus.OK);
	        	}
	        }
			
			Long id = adhesionService.saveAdhesion(adhesionData, rcFile, iceFile, ribFile);
			
			if(id != null) {
				response.put(Constants.ERRORS, new ArrayList<>());
				response.put(Constants.ID, id);
			} else {
				errors = new ArrayList<>();
				errors.add("Validation échouée !!");
				response.put(Constants.ERRORS, errors);
			}
			 
	        return new ResponseEntity<>(response, HttpStatus.OK);
		
		} catch(Exception e) {
			logger.error("Exception validateAdhesion ", e);
			return new ResponseEntity<>(response,HttpStatus.EXPECTATION_FAILED);
		}
		
    }
	
	@PostMapping("/printConvention")
	public void printConvention(@RequestBody Long idAdhesion, HttpServletResponse response) {
		
		byte[] fileOutput;
		try {
			fileOutput = adhesionService.printConvention(idAdhesion);
			
			response.setContentType("application/pdf");
			String fileName = "Convention.pdf";
			response.setHeader("Content-Disposition", "attachment;filename=" + fileName);
			response.getOutputStream().write(fileOutput);
			
		} catch (Exception e) {
			
			e.printStackTrace();
		} 
		
		
	}
	
	@PostMapping("/printBulletin")
	public void printBulletin(@RequestBody String idDocument, HttpServletResponse response) {
		
		byte[] fileOutput;
		try {
			fileOutput = adhesionService.printSignedBulletin(idDocument);
			
			response.setContentType("application/pdf");
			String fileName = "Bulletin-Signe.pdf";
			response.setHeader("Content-Disposition", "attachment;filename=" + fileName);
			response.getOutputStream().write(fileOutput);
			
		} catch (Exception e) {
			logger.error("printBulletin", e);
			e.printStackTrace();
		} 
		
		
	}
	
	
	
	@PostMapping("/uploadRC")
	public ResponseEntity<Map<String, Object>> uploadRC(@RequestParam("file") MultipartFile file) throws JMSException {
		Map<String, Object> response = adhesionService.uploadRC(file);
		try {
			this.rcFile = ArrayUtils.toObject(file.getBytes());
			tracerAction("uploadRC");
		} catch (IOException e) {
			e.printStackTrace();
		}
		
		return new ResponseEntity<>(response, HttpStatus.OK);
		
	}
	
	private void tracerAction(String action) throws JMSException {
		jmsTemplate.convertAndSend(Constants.message_queue, action);
	}
	
	@PostMapping("/uploadICE")
	public ResponseEntity<Map<String, Object>> uploadICE(@RequestParam("file") MultipartFile file) throws JMSException {
		
		Map<String, Object> response = adhesionService.uploadICE(file);
		
		try {
			this.iceFile = ArrayUtils.toObject(file.getBytes());
			tracerAction("uploadICE");
		} catch (IOException e) {
			e.printStackTrace();
		}
		
		return new ResponseEntity<>(response, HttpStatus.OK);
		
	}
	
	@PostMapping("/uploadRIB")
	public ResponseEntity<Map<String, Object>> uploadRIB(@RequestParam("file") MultipartFile file) throws JMSException {
		
		Map<String, Object> response = adhesionService.uploadRIB(file);
		
		try {
			this.ribFile = ArrayUtils.toObject(file.getBytes());
			tracerAction("uploadRIB");
		} catch (IOException e) {
			e.printStackTrace();
		}
		
		return new ResponseEntity<>(response, HttpStatus.OK);
		
	}
	
	@GetMapping("/generateCredentialsAdhesion")
    public ResponseEntity<String> generateCredentialsAdhesion(@RequestParam(name="id") Long idAdhesion){
		
		JSONObject response = new JSONObject();
		List<String> errors = new ArrayList<String>();
		Map<String, Object> credentials = adhesionService.generateCredentials(idAdhesion);
		
		try {
			if(credentials != null && !((String)credentials.get(Constants.LOGIN)).isEmpty() && !((String)credentials.get(Constants.PASSWORD)).isEmpty()) {
				
				response.put(Constants.LOGIN, (String)credentials.get(Constants.LOGIN));
				response.put(Constants.PASSWORD, (String)credentials.get(Constants.PASSWORD));
				
				return new ResponseEntity<String>(response.toString(), HttpStatus.OK);
			}
		
			errors.add("Une erreur est survenue lors de la génération du login/password.");
			response.put(Constants.ERRORS, errors);
		
		} catch (Exception e) {
			logger.error("Error in method generateCredentialsAdhesion : ", e);
			e.printStackTrace();
		}
		
		return new ResponseEntity<String>(response.toString(), HttpStatus.EXPECTATION_FAILED);
	}

	@GetMapping("/signBulletin")
	public ResponseEntity<Map<String, Object>> signBulletin(@RequestParam("id") Long idAdhesion) {
		
		Map<String, Object> response = new HashMap<String, Object>();
		try {
			response = adhesionService.signBulletin(idAdhesion);
			return new ResponseEntity<>(response, HttpStatus.OK);
			
		} catch (Exception e) {
			logger.error("Exception signBulletin ", e);
			return new ResponseEntity<>(response,HttpStatus.EXPECTATION_FAILED);
		} 
		
		
	}
}
